import 'package:flutter_web/material.dart';

class ScreenAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("test App bar"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.directions_car),
            onPressed: () {
              // _select(choices[0]);
            },
          ),
          IconButton(
            icon: Icon(Icons.portrait),
            onPressed: () {
              // _select(choices[0]);
            },
          ),
          IconButton(
            icon: Icon(Icons.publish),
            onPressed: () {
              // _select(choices[0]);
            },
          ),
        ],
      ),
      body: RaisedButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text('Go back!'),
      ),
    );
  }
}
