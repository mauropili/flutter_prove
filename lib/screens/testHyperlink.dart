import 'package:flutter_web/material.dart';
import 'dart:js' as js;

class ScreenHyperlink extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("test hyperlink")),
        body: ListView(
          children: <Widget>[
            Text('pippo'),
            // html.window.open('www.google.com','google'),
            FlatButton(
              child: Text("vai su gooogle"),
              onPressed: () {
                js.context.callMethod(
                    "open", ["http://www.google.com"]);
              },
            ),
            RaisedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Go back!'),
            ),
          ],
        ));
  }
}
