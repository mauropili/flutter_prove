import 'package:flutter_web/material.dart';
import 'counter_bloc.dart';
import 'counter_event.dart';

class ScreenSlicderBLoC extends StatefulWidget {
  @override
  _ScreenSlicderBLoCState createState() => _ScreenSlicderBLoCState();
}

class _ScreenSlicderBLoCState extends State<ScreenSlicderBLoC> {
  final _bloc = CounterBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider BLoC test'),
      ),
      body: Center(
        child: StreamBuilder(
          stream: _bloc.counter,
          initialData: 0,
          builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Valore massimo: ${_bloc.MaxCounter()}',
                ),
                Text(
                  'You have pushed the button this many times:',
                ),
                Text(
                  '${snapshot.data}',
                  style: Theme.of(context).textTheme.display1,
                ),

                Flexible(
                  flex: 1,
                  child: Slider(
                    min: 0.0,
                    max: 1.0 * _bloc.MaxCounter(),
                    onChanged: (nuovoValore) => _bloc.counterEventSink.add(ImpostaEvent(nuovoValore.round())), 
                    /* onChanged: (nuovoValore) {
                      // setState(() => _valore = nuovoValore);
                    }, */
                    value: 1.0 * snapshot.data,
                  ),
                ),

              ],
            );
          },
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            onPressed: () => _bloc.counterEventSink.add(IncrementEvent()),
            tooltip: 'Increment',
            child: Icon(Icons.add),
          ),
/*
          SizedBox(width: 10),
          FloatingActionButton(
            // onPressed: () => _bloc.counterEventSink.add(DecrementEvent()),
            tooltip: 'Decrement',
            child: Icon(Icons.remove),
          ),
*/
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _bloc.dispose();
  }
}
