abstract class CounterEvent {
  // messo nella classe astratta per poter poi essere implementato su ImpostaEvent
  GetValore() => 0;
}

class IncrementEvent extends CounterEvent {}

class DecrementEvent extends CounterEvent {}

// la classe ImpostaEvent viene chiamata passando un parametro
class ImpostaEvent extends CounterEvent {
  final int valore;

  @override
  GetValore() => valore;

  ImpostaEvent(this.valore);
}
