import 'dart:async';
import 'counter_event.dart';

class CounterBloc {
  int _contatore = 0;
  final int _contatoreMax = 47;

  MaxCounter() => _contatoreMax;

  final _contatoreStateController = StreamController<int>();

  // Scrive i cambiamenti di counter in questo sink
  StreamSink<int> get _inCounter => _contatoreStateController.sink;
  
  // For state, exposing only a stream which outputs data
  Stream<int> get counter => _contatoreStateController.stream;

  final _contatoreEventController = StreamController<CounterEvent>();
  
  // For events, exposing only a sink which is an input
  Sink<CounterEvent> get counterEventSink => _contatoreEventController.sink;

  CounterBloc() {
    // Whenever there is a new event, we want to map it to a new state
    _contatoreEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(CounterEvent event) {
    if (event is IncrementEvent) {
      if (_contatore < _contatoreMax) _contatore++;
    } else if (event is DecrementEvent) {
      if (_contatore > 0) _contatore--;
    } else {
      _contatore=event.GetValore();
    }
    // aggiunge il valore aggiornato del contatore allo _stateController attraverso il suoi sink.
    _inCounter.add(_contatore);
  }

  void dispose() {
    _contatoreStateController.close();
    _contatoreEventController.close();
  }
}
