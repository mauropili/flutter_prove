import 'package:flutter_web/material.dart';

class Scheda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("sign up"),
      ),
      body: Container(
          child: ListView(
            padding: const EdgeInsets.all(16.0),
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  // Navigate back to the first screen by popping the current route
                  // off the stack.
                  Navigator.pop(context);
                },
                child: Text('Go back!'),
              ),
            ])),
    );
  }
}