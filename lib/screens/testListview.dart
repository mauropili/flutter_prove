// ispirato da
// https://medium.com/@DakshHub/flutter-displaying-dynamic-contents-using-listview-builder-f2cedb1a19fb

import 'package:flutter_web/material.dart';

class ListDisplay extends StatefulWidget {
  @override
  State createState() => _DynamicList();
}

class _DynamicList extends State<ListDisplay> {
  List<String> litems = ['primo', '2', 'tre'];
  final TextEditingController eCtrl = TextEditingController();

  @override
  Widget build(BuildContext ctxt) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Test ListView.builder"),
      ),
      body: Column(
        children: <Widget>[
          TextField(
            controller: eCtrl,
            onSubmitted: (text) {
              litems.add(text);
              eCtrl.clear();
              setState(() {});
            },
          ),
          Expanded(
              child: ListView.builder(
                  itemCount: litems.length,
                  itemBuilder: (BuildContext ctxt, int Index) {
                    return Text(litems[Index]);
                  }))
        ],
      ),
    );
  }
}
