import 'package:flutter_web/material.dart';

class ScreenChip extends StatefulWidget {
  static const routeName = '/material/chip';
  @override
  State<StatefulWidget> createState() => _ChipDemoState();
}

class _ChipDemoState extends State<ScreenChip> {
  bool _filterChipSelected = false;
  bool _hasAvatar = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("test Chip")),
      body: _buildContents(),
    );
  }

  Widget _buildContents() {
    return Material(
        child: Column(
      children: [
        addPadding(Tooltip(
            child: Chip(
              label: Text('Chip'),
            ),
            message: 'Questo chip ha anche un tooltip')),
        addPadding(InputChip(
          label: Text('InputChip'),
        )),
        addPadding(ChoiceChip(
          label: Text('Selected ChoiceChip'),
          selected: true,
        )),
        addPadding(ChoiceChip(
          label: Text('Deselected ChoiceChip'),
          selected: false,
        )),
        Text('Su questi puoi cliccare:'),
        addPadding(FilterChip(
          label: Text('FilterChip'),
          selected: _filterChipSelected,
          onSelected: (bool newValue) {
            setState(() {
              _filterChipSelected = newValue;
            });
          },
        )),
        addPadding(ActionChip(
          label: Text('ActionChip'),
          onPressed: () {},
        )),
        addPadding(ActionChip(
          label: Text('Chip with avatar'),
          avatar: _hasAvatar
              ? CircleAvatar(
                  backgroundColor: Colors.amber,
                  child: Text('Z'),
                )
              : null,
          onPressed: () {
            setState(() {
              _hasAvatar = !_hasAvatar;
            });
          },
        )),
      ],
    ));
  }
}

Padding addPadding(Widget widget) => Padding(
      padding: EdgeInsets.all(10.0),
      child: widget,
    );
