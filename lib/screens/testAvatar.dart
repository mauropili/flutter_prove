import 'package:flutter_web/material.dart';

import 'avatarWidget.dart';

class ScreenAvatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("test AvatarWidget")),
      body: Stack(
        children: <Widget>[
          AvatarWidget(),
        ],
      ),
    );
  }
}
