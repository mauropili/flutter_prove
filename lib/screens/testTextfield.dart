import 'package:flutter_web/material.dart';

// Name
_inputNameField() => TextField(
      decoration: InputDecoration(
          filled: true,
          labelText: 'testo',
          hintText: 'Inserire un testo'),
    );

class ScreenTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("test TextField"),
      ),
      body: Container(
        child: ListView(
          padding: const EdgeInsets.all(16.0),
          children: <Widget>[
            _inputNameField(),
            RaisedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Go back!'),
            ),
          ]
        )
      ),
    );
  }
}
