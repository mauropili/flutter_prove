import 'package:flutter_web/material.dart';

import 'actionBar.dart';

class ScreenActionBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("test ActionBar")),
      body: Stack(
        children: <Widget>[
          ActionBar(),
        ],
      ),
    );
  }
}
