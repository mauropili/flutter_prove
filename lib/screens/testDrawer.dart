import 'package:flutter_web/material.dart';

class ScreenDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("test Drawer")),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Tab bar'),
              onTap: () {
                // Update the state of the app.
                Navigator.pushNamed(context, 'tabbar');
              },
            ),
            ListTile(
              title: Text('Form di registrazione'),
              onTap: () {
                // Update the state of the app.
                Navigator.pushNamed(context, 'register');
              },
            ),
          ],
        ),
      ),
      body: ListView(
        children: <Widget>[
          Text('premi sul burger menu'),
          RaisedButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text('Go back!'),
      ),
        ],
      )
      
    );
  }
}
