// esempio preso da 
// https://medium.com/@rkishan516/flutter-book-stepper-8a7de19977bd

import 'package:flutter_web/material.dart';

class ScreenStepper extends StatelessWidget {
  final _steps = <Step>[
    Step(
        title: Text("Step 1"),
        content: Container(
          padding: EdgeInsets.all(32.0),
          child: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                    hintText: "Ex. Abc",
                    labelText: "Name",
                    icon: Icon(Icons.contacts)),
              )
            ],
          ),
        ),
        isActive: true),
    Step(
        title: Text("Step 2"),
        content: Text("Do SomeThing"),
        isActive: true),
    Step(
        title: Text("Step 3"),
        content: Text("Do SomeThing"),
        isActive: true),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("test Stepper")),
      body: Stepper(
        steps: _steps,
      ),
    );
  }
}
