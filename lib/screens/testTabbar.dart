import 'package:flutter_web/material.dart';

_viewAutomobile() => Container(
  child: Column(
    children: <Widget>[
      Icon(Icons.directions_car),
      Text('Automobile')
    ],
  )
);


class ScreenTabbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.directions_car)),
                Tab(icon: Icon(Icons.directions_transit)),
                Tab(icon: Icon(Icons.directions_bike)),
              ],
            ),
            title: Text('test tab bar'),
          ),
          body: TabBarView(
            children: <Widget>[
              _viewAutomobile(),
              Icon(Icons.directions_transit),
              Icon(Icons.directions_bike),
            ],
          )),
    );
  }
}
