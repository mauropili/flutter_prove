import 'package:flutter_web/material.dart';

class AvatarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.favorite_border),
          ),
          IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(Icons.favorite_border),
          ),
          IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(Icons.favorite_border),
          ),
          Spacer(),
          IconButton(
              padding: EdgeInsets.zero,
              iconSize: 28.0,
              icon: Icon(Icons.favorite_border))
        ]);
  }
}
