import 'package:flutter_web/material.dart';

class ScreenBottomnav extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("test bottom navigation bar")),
      body: Container(),
      bottomNavigationBar:
          BottomNavigationBar(
            type: BottomNavigationBarType.shifting,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.add_circle, color: Color.fromARGB(255, 0, 0, 0)),
                  title: new Text('')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.add_comment, color: Color.fromARGB(255, 0, 0, 0)),
                  title: new Text('')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.ac_unit, color: Color.fromARGB(255, 0, 0, 0)),
                  title: new Text('')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.access_alarm, color: Color.fromARGB(255, 0, 0, 0)),
                  title: new Text(''))
            ]
          ),
    );
  }
}
