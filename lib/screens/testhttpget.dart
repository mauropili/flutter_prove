import 'package:flutter_web/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class ScreenHttpget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Battute su Chuck Norris")),
      body: FutureBuilder<String>(
        future: fetchJoke(),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              return Center(
                child: Text(snapshot.hasError ? snapshot.error : snapshot.data,
                    style: TextStyle(fontSize: 22.0)),
              );
          }
        },
      ),
    );
  }

  Future<String> fetchJoke() async {
    var url = "https://api.chucknorris.io/jokes/random?category=dev";
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      return jsonResponse['value'];
    } else {
      return ("Unexpected error occurred.");
    }
  }
}
