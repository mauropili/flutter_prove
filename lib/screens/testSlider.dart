import 'package:flutter_web/material.dart';

class ScreenSlider extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SliderDemoState();
}

class _SliderDemoState extends State<ScreenSlider> {
  double _valore = 33.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("test Slider")),
      body: _buildContents(),
    );
  }

  Widget _buildContents() {
    return Material(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Slider(
                min: 0.0,
                max: 100.0,
                onChanged: (nuovoValore) {
                  setState(() => _valore = nuovoValore);
                },
                value: _valore,
              ),
            ),
            Container(
              width: 50.0,
              alignment: Alignment.center,
              child: Text('${_valore.toInt()}',
                  style: Theme.of(context).textTheme.display1),
            ),
          ]),
    );
  }
}
