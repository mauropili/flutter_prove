import 'dart:async';
import 'blubox_event.dart';

// classe per avere i valori
class BluboxValori {
  int _larghezza;
  int _posLeft;
  final int _larghezzaMax;
  final int _posLeftMax;

  BluboxValori(
      this._larghezza, this._larghezzaMax, this._posLeft, this._posLeftMax);

  LarghezzaMax() => _larghezzaMax;
  PosLeftMax() => _posLeftMax;

  GetLarghezza() => _larghezza;
  SetLarghezza(v) {
    _larghezza = v;
  }

  IncrLarghezza() {
    if (_larghezza < _larghezzaMax) {
      _larghezza++;
    }
  }

  GetPosLeft() => _posLeft;
  SetPosLeft(v) => _posLeft = v;
}

// classe per la Business Logic
class BluboxBloc {
  BluboxValori _valori;

  BluboxBloc(larghezza, larghezzaMax, posLeft, posLeftMax) {
    // Ognivolta che c'è un nuovo evento, vogliamo mapparlo ad un nuovo state
    _bluboxEventController.stream.listen(_mapEventToState);
    _valori = BluboxValori(larghezza, larghezzaMax, posLeft, posLeftMax);
    // _valori.SetLarghezza(larghezza);
    // _valori.SetPosLeft(posLeft);
  }

  BluboxValori GetValori() => _valori;

  LarghezzaMax() => _valori.LarghezzaMax();
  PosLeftMax() => _valori.PosLeftMax();

  final _bluboxStateController = StreamController<BluboxValori>();

  // Scrive i cambiamenti di counter in questo sink
  StreamSink<BluboxValori> get _inBlubox => _bluboxStateController.sink;

  // Per lo state, espone solo uno stream con i dati
  Stream<BluboxValori> get dati => _bluboxStateController.stream;

  final _bluboxEventController = StreamController<BluboxEvent>();

  // Per gli eventi, espone solo un sink che è un input
  Sink<BluboxEvent> get bluboxEventSink => _bluboxEventController.sink;

  void _mapEventToState(BluboxEvent event) {
    if (event is AllargaEvent) {
      _valori.IncrLarghezza();
    } else if (event is LarghezzaSetEvent) {
      var l = event.GetLarghezza();
      _valori.SetLarghezza(l);
    } else if (event is PosleftSetEvent) {
      var p = event.GetPosizione();
      _valori.SetPosLeft(p);
    }
    // aggiunge il valore aggiornato del contatore allo _stateController attraverso il suoi sink.
    _inBlubox.add(_valori);
  }

  void dispose() {
    _bluboxStateController.close();
    _bluboxEventController.close();
  }
}
