abstract class BluboxEvent {
  // messo nella classe astratta per poter poi essere implementato su LarghezzaSetEvent
  GetLarghezza() => 0;
  GetPosizione() => 0;
}

class AllargaEvent extends BluboxEvent {}

// la classe LarghezzaSetEvent viene chiamata passando un parametro
class LarghezzaSetEvent extends BluboxEvent {
  final int valore;

  @override
  GetLarghezza() => valore;

  LarghezzaSetEvent(this.valore);
}


// la classe ImpostaEvent viene chiamata passando un parametro
class PosleftSetEvent extends BluboxEvent {
  final int valore;

  @override
  GetPosizione() => valore;

  PosleftSetEvent(this.valore);
}
