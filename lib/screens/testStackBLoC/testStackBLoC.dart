import 'package:flutter_web/material.dart';
import 'bluboxBLoC.dart';
import 'blubox_event.dart';

class ScreenStackBLoC extends StatefulWidget {
  @override
  _ScreenStackBLoCState createState() => _ScreenStackBLoCState();
}

class _ScreenStackBLoCState extends State<ScreenStackBLoC> {
  final _bloc = BluboxBloc(50, 200, 5, 100);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stack BLoC test'),
      ),
      body: StreamBuilder(
        stream: _bloc.dati,
        initialData: _bloc.GetValori(), // _valoreIniziale,
        builder: (BuildContext context, AsyncSnapshot<BluboxValori> snapshot) {
          return Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Dimensiona e posiziona il box blu con gli slider',
              ),
              Stack(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 200,
                    color: Colors.red,
                  ),
                  Container(
                    width: 190,
                    height: 190,
                    color: Colors.yellow,
                  ),
                  Positioned(
                    left: 1.0 * snapshot.data.GetPosLeft(),
                    child: Container(
                      width: 1.0 * snapshot.data.GetLarghezza(),
                      height: 80,
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
              Text(
                'Larghezza. Valore massimo: ${_bloc.LarghezzaMax()}',
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Slider(
                      min: 0.0,
                      max: 1.0 * _bloc.LarghezzaMax(),
                      onChanged: (nuovoValore) => _bloc.bluboxEventSink
                          .add(LarghezzaSetEvent(nuovoValore.round())),
                      value: 1.0 * snapshot.data.GetLarghezza(),
                    ),
                  ),
                  Container(
                    width: 80.0,
                    alignment: Alignment.center,
                    child: Text('${snapshot.data.GetLarghezza()}',
                        style: Theme.of(context).textTheme.display1),
                  ),
                ],
              ),
              Text(
                'Posizione sinistra. Valore massimo: ${_bloc.PosLeftMax()}',
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: Slider(
                      min: 0.0,
                      max: 1.0 * _bloc.PosLeftMax(),
                      onChanged: (nuovoValore) => _bloc.bluboxEventSink
                          .add(PosleftSetEvent(nuovoValore.round())),
                      value: 1.0 * snapshot.data.GetPosLeft(),
                    ),
                  ),
                  Container(
                    width: 80.0,
                    alignment: Alignment.center,
                    child: Text('${snapshot.data.GetPosLeft()}',
                        style: Theme.of(context).textTheme.display1),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            onPressed: () => _bloc.bluboxEventSink.add(AllargaEvent()),
            tooltip: 'Increment',
            child: Icon(Icons.add),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _bloc.dispose();
  }
}
