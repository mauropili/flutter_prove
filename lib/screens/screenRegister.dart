import 'package:flutter_web/material.dart';

// Color Converter
_hexToColor(String code) =>
    Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);

//TextStyle
_textStyle() => TextStyle();

//InputValidators
_ifNull(val) => val == null ? "Value is null" : "Value is empty";

// Name
_inputNameField() => TextFormField(
  decoration: const InputDecoration(
    labelText: 'User Name',
  ),
  validator: (val) => _ifNull(val),
);

// Location
_inputLocationField() => TextFormField(
      decoration: const InputDecoration(
        labelText: 'Location',
      ),
    );

class ScreenRegister extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Register"),
      ),
      body: MyCustomForm(),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  @override
  _MyCustomFormState createState() {
    return _MyCustomFormState();
  }
}

class _MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 80.0),
            Column(
              children: <Widget>[
                Icon(
                  Icons.add,
                  size: 30.0,
                ),
                SizedBox(height: 16.0),
                Text('Registrati'),
              ],
            ),
            SizedBox(height: 60.0),
            _inputNameField(),
            SizedBox(height: 8.0),
            _inputLocationField(),
            SizedBox(height: 8.0),
            RaisedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Go back!'),
            ),
          ]
      )
    );
  }
}
