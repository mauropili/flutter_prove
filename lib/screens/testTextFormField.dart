import 'package:flutter_web/material.dart';

//InputValidators
_ifNull(val) => val == null ? "Value is null" : "Value is empty";

// Name
_inputNameField() => TextFormField(
      decoration: const InputDecoration(
        labelText: 'Nome',
      ),
      // validator: (val) => _ifNull(val),
    );


class ScreenTextFormField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("test TextFormField"),
      ),
      body: Container(
          child: ListView(
            padding: const EdgeInsets.all(16.0),
            children: <Widget>[
              _inputNameField(),
              RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Go back!'),
              ),
            ])),
    );
  }
}
