import 'package:flutter_web/material.dart';

import 'screens/testScaffold.dart';
import 'screens/testText.dart';
import 'screens/testChip.dart';
import 'screens/screenRegister.dart';
import 'screens/testBottomNav.dart';
import 'screens/testDrawer.dart';
import 'screens/testListview.dart';
import 'screens/testStack.dart';
import 'screens/testTabbar.dart';
import 'screens/testTextfield.dart';
import 'screens/testTextFormField.dart';
import 'screens/testActionBar.dart';
import 'screens/testAvatar.dart';
import 'screens/testhttpget.dart';
import 'screens/testHyperlink.dart';
import 'screens/testStepper.dart';
import 'screens/testSlider.dart';
import 'screens/testRestApi.dart';
import 'screens/testBLoC/testBLoC.dart';
import 'screens/testSliderBLoC/testSliderBLoC.dart';
import 'screens/testStackBLoC/testStackBLoC.dart';
import 'screens/testAppBar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Generated App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: const Color(0xFF2196f3),
        accentColor: const Color(0xFF2196f3),
        canvasColor: const Color(0xFFfafafa),
        fontFamily: 'Titillium Web',
      ),
      routes: {
        'scaffold': (context) => ScreenScaffold(),
        'Text': (context) => ScreenText(),
        'Chip': (context) => ScreenChip(),
        'TextField': (context) => ScreenTextField(),
        'TextFormField': (context) => ScreenTextFormField(),
        'register': (context) => ScreenRegister(),
        'bottomnav': (context) => ScreenBottomnav(),
        'tabbar': (context) => ScreenTabbar(),
        'listview': (context) => ListDisplay(),
        'Appbar': (context) => ScreenAppBar(),
        'Drawer': (context) => ScreenDrawer(),
        'Stack': (context) => ScreenStack(),
        'ActionBar': (context) => ScreenActionBar(),
        'Avatar': (context) => ScreenAvatar(),
        'httpget': (context) => ScreenHttpget(),
        'Hyperlink': (context) => ScreenHyperlink(),
        'Stepper': (context) => ScreenStepper(),
        'Slider': (context) => ScreenSlider(),
        'RestApi': (context) => ScreenRestApi(),
        'BLoC': (context) => ScreenBLoC(),
        'SliderBLoC': (context) => ScreenSlicderBLoC(),
        'StackBLoC': (context) => ScreenStackBLoC(),
      },
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool vuota = false;

  Pulsante(context, testo, azione, [onpress]) => SizedBox(
      width: 240,
      child: RaisedButton(
        child: Text(testo),
        onPressed: () {
          Navigator.pushNamed(context, azione);
        },
      ));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter prove'),
      ),
      body: ListView(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          vuota
              ? Container()
              : Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                      RaisedButton(
                        child: Text('Svuota questa pagina'),
                        onPressed: () {
                          setState(() {
                            vuota = true;
                          });
                        },
                      ),
                      Pulsante(context, 'Pagina vuota', 'scaffold'),
                      Pulsante(context, 'Text', 'Text'),
                      Pulsante(context, 'Chip', 'Chip'),
                      Pulsante(context, 'Stack', 'Stack'),
                      Pulsante(context, 'TextField', 'TextField'),
                      Pulsante(context, 'TextFormField', 'TextFormField'),
                      Pulsante(context, 'Form di registrazione', 'register'),
                      Pulsante(context, 'Stepper', 'Stepper'),
                      Pulsante(context, 'Bottom navigation bar', 'bottomnav'),
                      Pulsante(context, 'Tab bar', 'tabbar'),
                      Pulsante(context, 'ListView.builder', 'listview'),
                      Pulsante(context, 'Appbar', 'Appbar'),
                      Pulsante(context, 'Drawer', 'Drawer'),
                      Pulsante(context, 'Action Bar', 'ActionBar'),
                      Pulsante(context, 'Avatar', 'Avatar'),
                      Pulsante(context, 'httpget', 'httpget'),
                      Pulsante(context, 'Hyperlink', 'Hyperlink'),
                      Pulsante(context, 'Slider', 'Slider'),
                      Pulsante(context, 'RestApi', 'RestApi'),
                      Pulsante(context, 'BLoC', 'BLoC'),
                      Pulsante(context, 'SliderBLoC', 'SliderBLoC'),
                      Pulsante(context, 'StackBLoC', 'StackBLoC'),
                    ]),
          vuota
              ? RaisedButton(
                  child: Text('Riempi questa pagina'),
                  onPressed: () {
                    setState(() {
                      vuota = false;
                    });
                  },
                  color: Colors.red[300],
                )
              : Container(),
          SizedBox(
            height: 20,
          ),
          vuota
              ? Text(
                  'La pagina è vuota',
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                )
              : Text(
                  'La pagina non è vuota',
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
        ],
      ),
    );
  }
}
